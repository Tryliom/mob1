import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, Alert, Button, ScrollView} from 'react-native';
import Utils from "../utils/Utils";

const styles = StyleSheet.create({
    container: {
        padding: 20
    },
    title: {
        fontSize: 20,
        marginBottom: 30
    }
});

class Register extends Component {
    constructor(props: any) {
        super(props);
    }

    state = {
        firstname: "",
        lastname: "",
        phonenumber: ""
    };

    register() {
        Utils.getAxios().post("api/user/apply", this.state)
            .then(data => {
                if (data.status === 200) {
                    alert("Inscription effectuée !");
                }
            })
            .catch((message) => {
                alert(message.toString());
            });
    }

    render() {
        // @ts-ignore
        return (
            <ScrollView style={styles.container}>
                <Text style={styles.title}>S'inscrire</Text>
                <Text>Prénom</Text>
                <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                    onChangeText={text => {this.setState({"firstname": text})}}
                    value={this.state.firstname}
                />
                <Text>Nom de famille</Text>
                <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                    onChangeText={text => {this.setState({"lastname": text})}}
                    value={this.state.lastname}
                />
                <Text>Numéro de téléphone</Text>
                <TextInput
                    // @ts-ignore
                    autocompletetype={"tel"}
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                    onChangeText={text => {this.setState({"phonenumber": text})}}
                    value={this.state.phonenumber}
                />
                <Button
                    // @ts-ignore
                    onPress={() => this.register()}
                    title={"Créer"}
                />
                <Button
                    // @ts-ignore
                    onPress={() => {
                        // @ts-ignore
                        this.props.navigation.replace("Login");
                    }} title={"Passer à la connexion"}
                />
            </ScrollView>
        );
    }

}

export default Register;