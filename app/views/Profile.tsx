import React, {Component} from 'react';
import {StyleSheet, View, Button, AsyncStorage, Text, ScrollView} from 'react-native';
import Utils from "../utils/Utils";

const styles = StyleSheet.create({
    container: {
        padding: 20,
        flex: 1
    },
    title: {
        fontSize: 20,
        marginBottom: 10,
        marginTop: 10
    }
});

class Profile extends Component {
    constructor(props: any) {
        super(props);
        AsyncStorage.getItem("token").then(value => {
            Utils.getAxios().get("api/me", {headers: {'Authorization': `Bearer ${value}`}})
                .then(data => {
                    const info = data.data.data;
                    this.setState({firstname: info.firstname, lastname: info.lastname, type: info.user_type});

                    Utils.getAxios().get("api/me/balance", {headers: {'Authorization': `Bearer ${value}`}})
                        .then(data => {
                            const info = data.data;
                            this.setState({debit: info.debit, credit: info.credit});
                        })
                        .catch(() => {
                            alert("Impossible de récupérer votre budget");
                        });
                })
                .catch(() => {
                    AsyncStorage.removeItem("token");
                    // @ts-ignore
                    this.props.navigation.replace("Login");
                });
        });
    }

    state = {
        firstname: "",
        lastname: "",
        debit: 0,
        credit: 0,
        type: 0
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <Text style={styles.title}>Prénom</Text>
                <Text>{this.state.firstname}</Text>
                <Text style={styles.title}>Nom</Text>
                <Text>{this.state.lastname}</Text>
                <Text style={styles.title}>Débit</Text>
                <Text>{this.state.debit}</Text>
                <Text style={styles.title}>Crédit</Text>
                <Text>{this.state.credit}</Text>
                {this.state.type == 1 &&
                    <Button title={"Validation du stock"} onPress={() => {
                        // @ts-ignore
                        this.props.navigation.replace("StockManager");
                    }} />
                }
                <Button title={"Panier"} onPress={() => {
                    // @ts-ignore
                    this.props.navigation.replace("Cart");
                }} />
                <Button title={"Déconnexion"} onPress={() => {
                    AsyncStorage.removeItem("token");
                    // @ts-ignore
                    this.props.navigation.replace("Login");
                }} />
            </ScrollView>
        );
    }

}

export default Profile;