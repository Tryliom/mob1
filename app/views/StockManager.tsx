import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, Button, Alert, AsyncStorage, Image, ScrollView} from 'react-native';
import Utils from "../utils/Utils";
import send from "expo-cli/build/commands/send";

const styles = StyleSheet.create({
    container: {
        padding: 20,
        flex: 1
    },
    title: {
        fontSize: 20,
        marginBottom: 30
    },
    logo: {
        width: 128,
        height: 128
    }
});

class StockManager extends Component {
    constructor(props: any) {
        super(props);
        AsyncStorage.getItem("token").then(value => {
            Utils.getAxios().get("api/products", {headers: {'Authorization': `Bearer ${value}`}})
                .then(data => {
                    const info = data.data.data;
                    for (let key in info) {
                        const elem = info[key];
                        Utils.getAxios().get("api/product/picture/"+elem.picture, {headers: {'Authorization': `Bearer ${value}`}})
                            .then(data => {
                                elem.pic = data.data;
                                this.setState({default_list: JSON.parse(JSON.stringify(info)), editList: JSON.parse(JSON.stringify(info))});
                            });
                    }
                })
                .catch(() => {
                    alert("Erreur avec la récupération des produits");
                });
        });
    }

    state = {
        default_list: [],
        editList: [],
        selectedProduct: 0,
        confirmedList: []
    };

    changeSelectedProduct(diff: number) {
        const {selectedProduct, editList} = this.state;
        let newSelected = selectedProduct + diff;
        if (newSelected < 0) {
            newSelected = editList.length-1;
        }
        if (newSelected === editList.length) {
            newSelected = 0;
        }

        this.setState({selectedProduct: newSelected});
    }

    validatingProduct() {
        const {selectedProduct, editList} = this.state;
        let tmpState = this.state;
        tmpState.confirmedList.push(editList[selectedProduct]);
        let tmpEditList: never[] = [];
        for (let k in editList) {
            // @ts-ignore
            if (k != selectedProduct)
                tmpEditList.push(editList[k]);
        }
        tmpState.editList = tmpEditList;
        if (selectedProduct > tmpEditList.length-1)
            tmpState.selectedProduct = tmpEditList.length-1;
        this.setState(tmpState);
    }

    generateEditStock() {
        const {editList, selectedProduct} = this.state;
        if (editList.length != 0) {
            const elem = editList[selectedProduct];
            // @ts-ignore
            return <View style={[{flexDirection:'row', alignItems:'center'}]}>
                <View style={{width: 200, marginRight: 10}}>
                    <Button title={"précédent"} onPress={() => {
                        this.changeSelectedProduct(-1);
                    }} />
                </View>
                <Image source={{
                    // @ts-ignore
                    uri: elem.pic
                }}
                       style={styles.logo}/>
                <View style={[{marginLeft: 10}]}>
                    <Text>{
                        // @ts-ignore
                        elem.name
                    }</Text>
                    <Text>En stock:
                        <TextInput
                            style={{ height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: "darkgrey", marginLeft: 10 }}
                            onChangeText={text => {
                                let tmpState = this.state;
                                // @ts-ignore
                                tmpState.editList[selectedProduct].stock = text<0 ? 0 : text;
                                this.setState(tmpState);
                            }}
                            value={
                                // @ts-ignore
                                elem.stock
                            }
                            keyboardType={'numeric'}
                        />
                    </Text>
                    <Text>Unité: {
                        // @ts-ignore
                        elem.unit
                    }</Text>
                </View>
                <View style={{width: 200, marginLeft: 10}}>
                    <Button title={"suivant"} onPress={() => {
                        this.changeSelectedProduct(1);
                    }} />
                </View>
                <View style={{width: 200, marginRight: 10}}>
                    <Button title={"quittancer"} onPress={() => {
                        this.validatingProduct();
                    }} />
                </View>
            </View>;
        }
    }

    generateConfirmStock() {
        const {confirmedList} = this.state;
        const resume = [];
        resume.push(
            <View key={100} style={{width: 200}}>
                <Button title={"Recommencer"} onPress={() => {
                    this.setState({editList: JSON.parse(JSON.stringify(this.state.default_list)), confirmedList: []});
                }} />
            </View>
        );
        resume.push(
            <View key={101} style={{width: 200}}>
                <Button title={"Enregistrer"} onPress={() => {
                    this.confirmProducts();
                }} />
            </View>
        );
        for (let k in confirmedList) {
            const elem = confirmedList[k];
            resume.push(
                <View key={k}>
                    <Text>{
                        // @ts-ignore
                        elem.name
                    }</Text>
                    <Text>{
                        // @ts-ignore
                        elem.stock
                    } {
                        // @ts-ignore
                        elem.unit
                    }</Text>
                </View>
            );
        }

        return resume;
    }

    confirmProducts() {
        // prepare list to send
        const {confirmedList} = this.state;
        let sendList = {quantities: []};
        for (let key in confirmedList) {
            let elem = confirmedList[key];
            sendList.quantities.push({
                // @ts-ignore
                id: elem.id,
                // @ts-ignore
                quantity: elem.stock
            });
        }
        // Send request
        AsyncStorage.getItem("token").then(value => {
            Utils.getAxios().post("api/products/stock", sendList, {headers: {'Authorization': `Bearer ${value}`}})
                .then(data => {
                    alert("Les quantités ont été enregistrées");
                })
                .catch(error => {
                    alert("Une erreur s'est produite");
                })
                .finally(() => {
                    // @ts-ignore
                    this.props.navigation.replace("StockManager");
                })
        });
    }

    needToDisplayConfirmedList() {
        const {editList, confirmedList} = this.state;
        return editList.length === 0 && confirmedList.length != 0;
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                {this.needToDisplayConfirmedList() ?
                    this.generateConfirmStock()
                    :
                    this.generateEditStock()}
                <Button title={"Profile"} onPress={() => {
                    // @ts-ignore
                    this.props.navigation.replace("Profile");
                }} />
                <Button title={"Déconnexion"} onPress={() => {
                    AsyncStorage.removeItem("token");
                    // @ts-ignore
                    this.props.navigation.replace("Login");
                }} />
            </ScrollView>
        );
    }

}

export default StockManager;