import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, Button, Alert, AsyncStorage, Image, ScrollView} from 'react-native';
import Utils from "../utils/Utils";

const styles = StyleSheet.create({
    container: {
        padding: 20,
        flex: 1
    },
    title: {
        fontSize: 20,
        marginBottom: 30
    },
    logo: {
        width: 128,
        height: 128
    }
});

class Cart extends Component {
    constructor(props: any) {
        super(props);
        AsyncStorage.getItem("token").then(value => {
            Utils.getAxios().get("api/products", {headers: {'Authorization': `Bearer ${value}`}})
                .then(data => {
                    const info = data.data.data;
                    for (let key in info) {
                        const elem = info[key];
                        Utils.getAxios().get("api/product/picture/"+elem.picture, {headers: {'Authorization': `Bearer ${value}`}})
                            .then(data => {
                                elem.pic = data.data;
                                this.setState({list: info});
                            });
                    }
                })
                .catch(() => {
                    alert("Erreur avec la récupération des produits");
                });
        });
    }

    state = {
        list: []
    };

    generateProductTabs() {
        const {list} = this.state;
        const comp = [];
        if (list != []) {
            for (let key in list) {
                const elem = list[key];
                // @ts-ignore
                comp.push(<View key={key} style={[{flexDirection:'row', alignItems:'center'}]}>
                    <Image source={{
                        // @ts-ignore
                        uri: elem.pic
                    }}
                    style={styles.logo}/>
                    <View style={[{marginLeft: 10}]}>
                        <Text>{
                            // @ts-ignore
                            elem.name
                        }</Text>
                        <Text>En stock: {
                            // @ts-ignore
                            elem.stock
                        }</Text>
                        <Text>Prix: {
                            // @ts-ignore
                            elem.price
                        } chf le {
                            // @ts-ignore
                            elem.unit
                        }</Text>
                        <Text>Dernière mise à jour: {
                            // @ts-ignore
                            elem.updated_at
                        }</Text>
                    </View>
                </View>);
            }
        }

        return <ScrollView>{comp}</ScrollView>;
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                {this.generateProductTabs()}
                <Button title={"Profile"} onPress={() => {
                    // @ts-ignore
                    this.props.navigation.replace("Profile");
                }} />
                <Button title={"Déconnexion"} onPress={() => {
                    AsyncStorage.removeItem("token");
                    // @ts-ignore
                    this.props.navigation.replace("Login");
                }} />
            </ScrollView>
        );
    }

}

export default Cart;