import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, Button, Alert, AsyncStorage, ScrollView} from 'react-native';
import Utils from "../utils/Utils";

const styles = StyleSheet.create({
    container: {
        padding: 20
    },
    title: {
        fontSize: 20,
        marginBottom: 30
    }
});

class Login extends Component {
    constructor(props: any) {
        super(props);
    }

    state = {
        token: ""
    };

    login() {
        // Validate the token
        if (/[a-zA-Z0-9]{60}/.test(this.state.token)) {
            AsyncStorage.setItem("token", this.state.token);
            Utils.getAxios().get("api/me", {headers: {'Authorization': `Bearer ${this.state.token}`}})
                .then(() => {
                    let info = "Login réussi !";
                    alert(info);
                    // @ts-ignore
                    this.props.navigation.replace("Profile");
                })
                .catch(() => {
                    const message = "Token erronée";
                    alert(message);
                });
        } else {
            const message = "Veuillez rentrer une token valide";
            alert(message);
        }
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <Text style={styles.title}>Connexion</Text>
                <Text>Token</Text>
                <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1 }}
                    onChangeText={text => {this.setState({"token": text})}}
                    value={this.state.token}
                />
                <Button title={"Connexion"} onPress={() => this.login()} />
                <Button title={"S'inscrire"} onPress={() =>
                    // @ts-ignore
                    this.props.navigation.replace("Register")
                } />
            </ScrollView>
        );
    }

}

export default Login;