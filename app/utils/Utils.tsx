// @ts-ignore
import axios from "axios";

const Utils = {
    getAxios: function() {
        return axios.create({
            baseURL: "http://127.0.0.1:8000"
        });
    }
};

export default Utils;