import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, Button, Alert, AsyncStorage} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Register from './app/views/Register';
import Login from './app/views/Login';
import Profile from './app/views/Profile';
import Cart from "./app/views/Cart";
import StockManager from "./app/views/StockManager";

const Stack = createStackNavigator();

class App extends Component {

    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator headerMode={"float"}>
                    <Stack.Screen name="Profile" component={Profile}/>
                    <Stack.Screen name="Cart" component={Cart}/>
                    <Stack.Screen name="StockManager" component={StockManager}/>
                    <Stack.Screen name="Login" component={Login}/>
                    <Stack.Screen name="Register" component={Register}/>
                </Stack.Navigator>
            </NavigationContainer>
        );
    }

}

export default App;
